package buu.informatics.s59160106.dota2guidever2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.GridView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_selecthero.*

class Selecthero : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selecthero)

        val gridview = this.findViewById(R.id.gridview) as GridView
        val adapter = custom_adapter(this, R.layout.row_data, data)
        gridview.adapter = adapter
    }

    val data : ArrayList<row_data>
    get(){
        val item_liste : ArrayList<row_data> = ArrayList<row_data>()
        item_liste.add(row_data(R.drawable.axe,"Axe"))
        item_liste.add(row_data(R.drawable.antimage,"Aniti-Mage"))
        item_liste.add(row_data(R.drawable.alchemist,"Alchemist"))
        item_liste.add(row_data(R.drawable.bane,"Bane"))
        item_liste.add(row_data(R.drawable.clinkz,"Clinkz"))
        item_liste.add(row_data(R.drawable.riki,"Riki"))


        gridview.setOnItemClickListener { parent, view, position, id ->

            Toast.makeText(this, "Clicked item :"+" "+position,Toast.LENGTH_SHORT).show()
            if(position == 0){
                val intent = Intent(this,HeroInfoActivity::class.java)
                startActivity(intent)
            }
            else if(position == 1){
                val intent = Intent(this,HeroAMInfo::class.java)
                startActivity(intent)
            }
            else if(position == 2){
                val intent = Intent(this,HeroAlcheInfo::class.java)
                startActivity(intent)
            }
            else if(position == 3){
                val intent = Intent(this,HeroBaneInfo::class.java)
                startActivity(intent)
            }
            else if(position == 4){
                val intent = Intent(this,HeroClinkzInfo::class.java)
                startActivity(intent)
            }
            else if(position == 5){
                val intent = Intent(this,HeroRikiInfo::class.java)
                startActivity(intent)
            }


        }
        return item_liste
    }

}
