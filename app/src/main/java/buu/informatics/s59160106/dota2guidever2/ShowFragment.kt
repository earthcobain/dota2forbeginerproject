package buu.informatics.s59160106.dota2guidever2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ShowFragment : AppCompatActivity() {
    var isFragmentOneLoaded = true;
    val manager = supportFragmentManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_fragment)

        val Change = findViewById<Button>(R.id.fragmentCC_btn)
        Change.setOnClickListener({
            if(isFragmentOneLoaded )
                ShowFragmentTwo()
            else
                ShowFragmentOne()
        })
    }
    fun ShowFragmentOne(){
        val transaction = manager.beginTransaction()
        val fragment = FragmentOne()
        transaction.replace(R.id.fragment_holder1,fragment)
        transaction.addToBackStack(null)
        transaction.commit()
        isFragmentOneLoaded = true
    }
    fun ShowFragmentTwo(){
        val transaction = manager.beginTransaction()
        val fragment = FragmentTwo()
        transaction.replace(R.id.fragment_holder1,fragment)
        transaction.addToBackStack(null)
        transaction.commit()
        isFragmentOneLoaded = false
    }
    }

