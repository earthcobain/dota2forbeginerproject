package buu.informatics.s59160106.dota2guidever2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        guide_button.setOnClickListener {
            val intent = Intent(this, Selecthero::class.java)
            startActivity(intent)
        }
        about_button.setOnClickListener {
            val intent = Intent(this, AboutActivity::class.java)
            startActivity(intent)
        }
        fragment_btn.setOnClickListener {
            val intent = Intent(this, ShowFragment::class.java)
            startActivity(intent)
        }
    }
}
