package buu.informatics.s59160106.dota2guidever2

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView

class custom_adapter(private val getContext: Context,private val CustomLayoutId : Int,private val custom_item :ArrayList<row_data>):
    ArrayAdapter<row_data>(getContext, CustomLayoutId, custom_item) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var  row = convertView
        val Holder : ViewHolder

        if(row == null) {

            val inflater = (getContext as Activity).layoutInflater

            row = inflater!!.inflate(CustomLayoutId, parent, false)

            Holder = ViewHolder()

            Holder.herojpg = row!!.findViewById(R.id.herojpg) as ImageView
            Holder.heroname = row!!.findViewById(R.id.heroname) as TextView

            row.setTag(Holder)

        }else{

            Holder = row.getTag() as ViewHolder
        }

            val item = custom_item[position]

            Holder.herojpg!!.setImageResource(item.image)
            Holder.heroname!!.setText(item.text)

            return row

    }
    class ViewHolder
    {
        internal var herojpg : ImageView? = null
        internal var heroname : TextView? = null
    }

}